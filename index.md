---
title: Entities
template: main-full.html
---

Each "thing" that you want to represent in your database is called an "Entity". Each entity has attributes, the data that describes it.

In a **Relational Database**, each entity is represented by one row in a database table. The attributes are held in columns of the table.

For these examples, we'll define "Person", "Birth Certificate", "Organ" and "Address" entities with the following attributes. 

In the "Key" column, you'll see three possible values:

   * (blank) the attribute is not a key
   * PK - represents a Primary Key, a value that must be unique in the table storing the entity. This is typically a unique identifier for an entity.
   * FK - represents a Foreign Key, a primary key in some other table that we're associated with.

In this example, I'm representing primary keys using Strings. I chose Strings here because mobile applications may synchronize their data between mobile devices and servers (or even other mobile devices). If entities can be created on mobile devices, possibly while not connected to a server, we can use a UUID ([Universally Unique Identifier](https://en.wikipedia.org/wiki/Universally_unique_identifier)) to ensure we don't have collisions between ids created on different mobile devices at the same time. We'll see examples of this later.

## Person

| Attribute | Type   | Key | Comment                |
|-----------|--------|-----|------------------------|
| id        | String | PK  | Unique identifier      |
| name      | String |     | name of the person     |
| ssn       | String |     | social security number |

## Organ

| Attribute | Type   | Key | Comment             |
|-----------|--------|-----|---------------------|
| id        | String | PK  | Unique identifier   |
| personId  | String | FK  | id of owning person |
| type      | String |     | type of organ       |

## Birth Certificate

| Attribute   | Type   | Key | Comment              |
|-------------|--------|-----|----------------------|
| id          | String | PK  | Unique identifier    |
| personId    | String | FK  | id of owning person  |
| timeOfBirth | long   |     | birth time in millis |

## Address

| Attribute | Type   | Key | Comment           |
|-----------|--------|-----|-------------------|
| id        | String | PK  | Unique identifier |
| street    | String |     |                   |
| city      | String |     |                   |
| state     | String |     |                   |
| zip       | String |     |                   |
